FROM openjdk:12-jdk-oracle

COPY crm-app.jar crm-app.jar

ENTRYPOINT ["java","-jar","/crm-app.jar"]