package com.villacarte.crm.infrastructure.service;

import com.villacarte.crm.domain.model.User;
import com.villacarte.crm.domain.repository.UserRepository;
import com.villacarte.crm.domain.service.InitChecks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

import java.util.List;

@Controller
public class InitChecksImpl implements InitChecks {

    @Autowired
    UserRepository userRepository;

    @Override
    public void checkUsers() {
        List<User> users = userRepository.findAll();
        if (users.isEmpty()) {
            User admin = User.Factory.create("admin");
            admin.setPassword("admin");
            admin.setFirstName("Админ");
            admin.setLastName("Админович");
            userRepository.save(admin);
        }
    }

}
