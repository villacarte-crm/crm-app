package com.villacarte.crm.infrastructure.repository.jpa.user;

import com.villacarte.crm.domain.model.User;
import com.villacarte.crm.domain.repository.UserRepository;
import com.villacarte.crm.domain.repository.UserSearchFilter;
import com.villacarte.crm.domain.repository.common.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Stream;

@Service
public class UserRepositoryImpl implements UserRepository {

    @Autowired
    JpaUserRepository jpaUserRepository;

    @Override
    public User get(String id, boolean withPhoto) {

        if (withPhoto)
            return JpaUserFactory.createWithBigPhoto(jpaUserRepository.findById(id).orElse(null));
        else
            return JpaUserFactory.create(jpaUserRepository.findById(id).orElse(null));
    }

    @Override
    public User findByUsername(String username) {

        Optional<JpaUser> jpaUser = jpaUserRepository.findByUsername(username);

        return JpaUserFactory.create(jpaUser.orElse(null));
    }

    @Override
    public List<User> findAll() {
        return JpaUserFactory.create(jpaUserRepository.findAll());
    }

    @Override
    public Stream<User> search(int offset, int limit, Map<String, SortOrder> sortOrders) {
        Pageable pageable = PageRequest.of(offset / limit, limit, makeSort(sortOrders));
        return JpaUserFactory.create(jpaUserRepository.findAll(pageable).get());
    }

    @Override
    public Stream<User> search(int offset, int limit, Map<String, SortOrder> sortOrders, UserSearchFilter filter) {
        if (filter == null || filter.getSearchParams().size() == 0) {
            return search(offset, limit, sortOrders);
        } else {
            Pageable pageable = PageRequest.of(offset / limit, limit, makeSort(sortOrders));
            JpaUserSearchFilter jpaFilter = JpaUserSearchFilter.of(filter);
            return JpaUserFactory.create(jpaUserRepository.findAll(jpaFilter, pageable).get());
        }
    }


    @Override
    public List<User> search(String searchString) {
        return JpaUserFactory.create(jpaUserRepository.search(searchString));
    }

    @Override
    public void save(User user) {
        JpaUser jpaUser = jpaUserRepository.findById(user.getId()).orElse(null);
        if (jpaUser == null)
            jpaUser = JpaUserFactory.createSqlWithPhoto(user);
        else
            jpaUser = JpaUserFactory.updateSql(jpaUser, user);
        jpaUserRepository.save(jpaUser);
    }

    @Override
    public void delete(User user) {
        jpaUserRepository.deleteById(user.getId());
    }

    @Override
    public Integer count(UserSearchFilter filter) {
        if (filter == null)
            return Math.toIntExact(jpaUserRepository.count());
        else
            return Math.toIntExact(jpaUserRepository.count(JpaUserSearchFilter.of(filter)));
    }

    private Sort makeSort(Map<String, SortOrder> sortOrders) {
        List<Sort.Order> orders = new LinkedList<>();
        sortOrders.forEach((param, sortOrder) -> {
            switch (sortOrder) {
                case ASC: orders.add(Sort.Order.asc(param));
                case DESC: orders.add(Sort.Order.desc(param));
            }
        });
        return Sort.by(orders);
    }

}
