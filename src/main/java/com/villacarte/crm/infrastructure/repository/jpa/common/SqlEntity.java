package com.villacarte.crm.infrastructure.repository.jpa.common;

import com.villacarte.crm.domain.model.User;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ENTITY")
public class SqlEntity {

    @Id
    @Column(name = "ID", nullable = false)
    private String id;
    public String getId() { return id; }
    public void setId(String id) { this.id = id; }

    @Column(name = "CREATED", nullable = false)
    private Date created;
    public Date getCreated() { return created; }
    public void setCreated(Date created) { this.created = created; }

    /*
    @ManyToOne
    @JoinColumn(name = "CREATED_BY_ID")
    private User createdBy;
    public User getCreatedBy() { return createdBy; }
    public void setCreatedBy(User createdBy) { this.createdBy = createdBy; }

     */
}
