package com.villacarte.crm.infrastructure.repository.jpa.user;

import com.villacarte.crm.domain.model.User;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

public class JpaUserFactory {

    public static User create(JpaUser jpaUser) {
        if (jpaUser == null) return null;
        User user = User.Factory.restore(jpaUser.getId(), jpaUser.getUsername(), jpaUser.getCreated());
        user.setPasswordHash(jpaUser.getPasswordHash());
        user.setFirstName(jpaUser.getFirstName());
        user.setLastName(jpaUser.getLastName());
        return user;
    }

    public static User createWithBigPhoto(JpaUser jpaUser) {
        User user = create(jpaUser);
        if (user != null)
            user.setBigPhoto(jpaUser.getBigPhoto());
        return user;
    }

    public static List<User> create(Iterable<JpaUser> jpaUsers) {
        if (jpaUsers == null) return null;
        List<User> users = new LinkedList<>();
        for (JpaUser jpaUser : jpaUsers) {
            users.add(create(jpaUser));
        }
        return users;
    }

    public static JpaUser createSql(User user) {
        if (user == null) return null;
        JpaUser jpaUser = new JpaUser();
        jpaUser.setId(user.getId());
        jpaUser.setUsername(user.getUsername());
        jpaUser.setFirstName(user.getFirstName());
        jpaUser.setLastName(user.getLastName());
        jpaUser.setCreated(user.getCreated());
        jpaUser.setPasswordHash(user.getPasswordHash());
        return jpaUser;
    }

    public static JpaUser createSqlWithPhoto(User user) {
        JpaUser jpaUser = createSql(user);
        if (jpaUser != null)
            jpaUser.setBigPhoto(user.getBigPhoto());
        return jpaUser;
    }

    public static JpaUser updateSql(JpaUser jpaUser, User user) {
        if (jpaUser == null || user == null) return null;
        jpaUser.setUsername(user.getUsername());
        jpaUser.setFirstName(user.getFirstName());
        jpaUser.setLastName(user.getLastName());
        jpaUser.setCreated(user.getCreated());
        jpaUser.setPasswordHash(user.getPasswordHash());
        jpaUser.setBigPhoto(user.getBigPhoto());
        return jpaUser;
    }

    public static Stream<User> create(Stream<JpaUser> jpaUserStream) {
        Stream.Builder<User> userStreamBuilder = Stream.builder();
        jpaUserStream.forEach(jpaUser ->
                userStreamBuilder.accept(JpaUserFactory.create(jpaUser))
        );
        Stream<User> userStream = userStreamBuilder.build();
        return userStream;
    }

}