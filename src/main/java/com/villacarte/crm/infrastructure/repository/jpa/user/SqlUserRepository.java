package com.villacarte.crm.infrastructure.repository.jpa.user;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SqlUserRepository extends JpaRepository<JpaUser, String> {
}
