package com.villacarte.crm.infrastructure.repository.jpa.user;

import com.villacarte.crm.domain.repository.UserSearchFilter;
import com.villacarte.crm.domain.repository.common.*;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class JpaUserSearchFilter implements Specification<JpaUser> {

    private UserSearchFilter filter;

    private JpaUserSearchFilter(UserSearchFilter filter) {
        this.filter = filter;
    }

    public static JpaUserSearchFilter of(UserSearchFilter filter) {
        return new JpaUserSearchFilter(filter);
    }

    @Override
    public Predicate toPredicate(Root<JpaUser> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {

        List<Predicate> predicates = new ArrayList<>();

        filter.getSearchParams().forEach((s, param) -> {
            if (param instanceof IdSearchParam)
                predicates.add(addIdSearchParam((IdSearchParam)param, root, criteriaBuilder));

            if (param instanceof StringSearchParam)
                predicates.add(addStringSearchParam((StringSearchParam)param, root, criteriaBuilder));

            if (param instanceof DateSearchParam)
                predicates.add(addDateSearchParam((DateSearchParam)param, root, criteriaBuilder));

            if (param instanceof IntSearchParam)
                predicates.add(addIntSearchParam((IntSearchParam)param, root, criteriaBuilder));
        });

        if (predicates.size() == 0)
            return null;
        else
            return predicates.size() > 1
                    ? criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]))
                    : predicates.get(0);
    }

    private Predicate addIdSearchParam(IdSearchParam param, Root<JpaUser> root, CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.equal(root.get(param.getParam()), param.getSearchId());
    }

    private Predicate addStringSearchParam(StringSearchParam param, Root<JpaUser> root, CriteriaBuilder criteriaBuilder) {
        if (param.getSearchType() == StringSearchParam.SearchType.EQUALS) {
            return criteriaBuilder.equal(root.get(param.getParam()), param.getSearchString());
        } else {
            return criteriaBuilder.like(root.get(param.getParam()), "%" + param.getSearchString() + "%");
        }
    }

    private Predicate addDateSearchParam(DateSearchParam param, Root<JpaUser> root, CriteriaBuilder criteriaBuilder) {
        if (param.getSearchType() == DateSearchParam.SearchType.EXACT) {
            return criteriaBuilder.equal(root.get(param.getParam()), param.getSingleDate());
        } else if (param.getSearchType() == DateSearchParam.SearchType.AFTER) {
            return criteriaBuilder.greaterThan(root.get(param.getParam()), param.getAfterDate());
        } else if (param.getSearchType() == DateSearchParam.SearchType.BEFORE) {
            return criteriaBuilder.lessThan(root.get(param.getParam()), param.getBeforeDate());
        } else if (param.getSearchType() == DateSearchParam.SearchType.PERIOD) {
            return criteriaBuilder.between(root.get(param.getParam()), param.getAfterDate(), param.getBeforeDate());
        } else {
            return null;
        }
    }

    private Predicate addIntSearchParam(IntSearchParam param, Root<JpaUser> root, CriteriaBuilder criteriaBuilder) {
        if (param.getSearchType() == IntSearchParam.SearchType.EQUALS) {
            return criteriaBuilder.equal(root.get(param.getParam()), param.getSearchValue());
        } else if (param.getSearchType() == IntSearchParam.SearchType.MORE) {
            return criteriaBuilder.greaterThan(root.get(param.getParam()), param.getSearchValue());
        } else if (param.getSearchType() == IntSearchParam.SearchType.LESS) {
            return criteriaBuilder.lessThan(root.get(param.getParam()), param.getSearchValue());
        } else {
            return null;
        }
    }

}
