package com.villacarte.crm.infrastructure.repository.jpa.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface JpaUserRepository extends JpaRepository<JpaUser, String>, JpaSpecificationExecutor<JpaUser> {

    @Query(value = "SELECT u FROM JpaUser u WHERE u.username = :username")
    Optional<JpaUser> findByUsername(@Param("username") String username);

    @Query("SELECT u FROM JpaUser u WHERE (LOWER(u.username) LIKE %:searchString%) OR " +
            "(LOWER(u.firstName) LIKE %:searchString%) OR " +
            "(LOWER(u.lastName) LIKE %:searchString%)")
    List<JpaUser> search(@Param("searchString") String searchString);
}