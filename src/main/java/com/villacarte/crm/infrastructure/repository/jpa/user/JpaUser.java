package com.villacarte.crm.infrastructure.repository.jpa.user;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Date;

@Entity
@Table(name = "USER"/*, indexes = {
        @Index(name = "USER_USERNAME_IND1", columnList = "USERNAME", unique = true)
}*/)
@FilterDef(name="filterByFirstName", parameters={@ParamDef(name="firstName", type="string")})
@Filters( {
        @Filter(name="filterByFirstName", condition=":firstName = firstName")
} )
public class JpaUser {

    @Id
    @Column(name = "ID", nullable = false)
    private String id;
    public String getId() { return id; }
    public void setId(String id) { this.id = id; }

    @Column(name = "CREATED", nullable = false)
    private Date created;
    public Date getCreated() { return created; }
    public void setCreated(Date created) { this.created = created; }

    @Column(name = "USERNAME", nullable = false, unique = true)
    private String username;
    public String getUsername() { return username; }
    public void setUsername(String username) { this.username = username; }

    @Column(name = "PASSWORD_HASH")
    private String passwordHash;
    public String getPasswordHash() { return passwordHash; }
    public void setPasswordHash(String passwordHash) { this.passwordHash = passwordHash; }

    @Column(name = "FIRST_NAME")
    private String firstName;
    public String getFirstName() { return firstName; }
    public void setFirstName(String firstName) { this.firstName = firstName; }

    @Column(name = "LAST_NAME")
    private String lastName;
    public String getLastName() { return lastName; }
    public void setLastName(String lastName) { this.lastName = lastName; }


    @Basic(fetch = FetchType.LAZY)
    @Lob @Column(name = "BIG_PHOTO")
    private byte[] bigPhoto;
    public byte[] getBigPhoto() { return bigPhoto; }
    public void setBigPhoto(byte[] bigPhoto) {
        this.bigPhoto = bigPhoto;
    }

}
