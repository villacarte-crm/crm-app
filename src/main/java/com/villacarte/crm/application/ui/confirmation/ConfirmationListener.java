package com.villacarte.crm.application.ui.confirmation;

@FunctionalInterface
public interface ConfirmationListener<T extends Object> {
    void onConfirmationEvent(T object);
}