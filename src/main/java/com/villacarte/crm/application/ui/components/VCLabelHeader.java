package com.villacarte.crm.application.ui.components;

import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.html.Label;

public class VCLabelHeader extends Label {

    public enum Size {
        SMALL,
        MEDIUM,
        LARGE,
        XLARGE
    }

    private VCLabelHeader(String text, Size size, HasComponents parent) {
        super(text);

        switch (size) {
            case SMALL: break;
            case MEDIUM: setClassName("vc-label-header-medium");
            case LARGE: break;
            case XLARGE: setClassName("vc-label-header-xlarge");
        }

        parent.add(this);
    }

    public static VCLabelHeader create(String text, Size size, HasComponents parent) {
        return new VCLabelHeader(text, size, parent);
    }

}
