package com.villacarte.crm.application.ui.confirmation;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

public class ConfirmationDialog<T extends Object> extends Dialog {

    private Label text;
    private Button yesBtn;
    private Button noBtn;
    private T item;

    public ConfirmationDialog(ConfirmationListener<T> yesEvent, ConfirmationListener<T> noEvent) {

        setWidth("450px");

        this.text = new Label();
        add(this.text);

        HorizontalLayout buttonsLayout = new HorizontalLayout();
        buttonsLayout.setWidthFull();
        buttonsLayout.setJustifyContentMode(FlexComponent.JustifyContentMode.END);
        add(buttonsLayout);

        yesBtn = new Button("Да");
        yesBtn.addThemeVariants(ButtonVariant.LUMO_SUCCESS, ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_SMALL);
        yesBtn.addClickListener(event -> {
            if (yesEvent != null)
                yesEvent.onConfirmationEvent(item);
            close();
        });
        buttonsLayout.add(yesBtn);

        noBtn = new Button("Нет");
        noBtn.addThemeVariants(ButtonVariant.LUMO_ERROR, ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_SMALL);
        noBtn.addClickListener(event -> {
            if (noEvent != null)
                noEvent.onConfirmationEvent(item);
            close();
        });
        buttonsLayout.add(noBtn);

        setOpened(false);
    }

    private void fill(String text, T item) {
        this.text.setText(text);
        this.item = item;
    }

    public void open(String text, T item) {
        fill(text, item);
        open();
    }

}