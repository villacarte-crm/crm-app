package com.villacarte.crm.application.ui.users;

import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.theme.NoTheme;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;
import com.vaadin.flow.theme.material.Material;
import com.villacarte.crm.application.ui.layout.MainLayout;

@Route(value = "users", layout = MainLayout.class)
@HtmlImport("styles/user-administration.html")
@HtmlImport("styles/common.html")
public class UserAdministrationView extends VerticalLayout {

    UsersGrid usersGrid;
    UserForm userForm;

    public UserAdministrationView() {

        usersGrid = new UsersGrid();
        userForm = new UserForm();
        usersGrid.setUserForm(userForm);
        userForm.setUsersGrid(usersGrid);
        add(usersGrid);
        add(userForm);
        userForm.setVisible(false);
        usersGrid.setVisible(true);
    }

}
