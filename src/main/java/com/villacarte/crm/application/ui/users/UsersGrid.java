package com.villacarte.crm.application.ui.users;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.grid.HeaderRow;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.textfield.TextFieldVariant;
import com.vaadin.flow.data.provider.*;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.villacarte.crm.application.auth.CurrentUser;
import com.villacarte.crm.application.ui.confirmation.ConfirmationDialog;
import com.villacarte.crm.application.ui.util.ApplicationContextProvider;
import com.villacarte.crm.domain.model.User;
import com.villacarte.crm.domain.repository.UserRepository;
import com.villacarte.crm.domain.repository.UserSearchFilter;
import com.villacarte.crm.domain.repository.common.SortOrder;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class UsersGrid extends VerticalLayout {

    private UserForm userForm;
    public void setUserForm (UserForm userForm) { this.userForm = userForm; }

    private Grid<User> grid;
    private ConfirmationDialog<User> deleteConfirmation;

    UserSearchFilter userSearchFilter;

    public UsersGrid() {

        setSizeFull();

        deleteConfirmation = new ConfirmationDialog<>(this::deleteUser, null);
        add(deleteConfirmation);

        grid = new Grid<>();
        grid.setSizeFull();
        grid.setMultiSort(true);

        BackEndDataProvider<User, UserSearchFilter> dataProvider =
                DataProvider.fromFilteringCallbacks(query -> fetch(query), query -> count(query));

        ConfigurableFilterDataProvider<User, Void, UserSearchFilter> cdp = dataProvider.withConfigurableFilter();
        userSearchFilter = UserSearchFilter.create();
        cdp.setFilter(userSearchFilter);
        grid.setDataProvider(cdp);

        grid.addThemeVariants(GridVariant.LUMO_NO_BORDER);

        Grid.Column<User> usernameColumn = grid.addColumn(User::getUsername).setHeader("Имя пользователя")
                .setSortable(true).setSortProperty("username").setFlexGrow(1);
        Grid.Column<User> firstNameColumn = grid.addColumn(User::getFirstName).setHeader("Имя")
                .setSortable(true).setSortProperty("firstName").setFlexGrow(1);
        Grid.Column<User> lastNameColumn = grid.addColumn(User::getLastName).setHeader("Фамилия")
                .setSortable(true).setSortProperty("lastName").setFlexGrow(1);
        Grid.Column<User> buttonsColumn = grid.addComponentColumn(user -> createUserButtons(user))
                .setFlexGrow(0).setWidth("100px").setResizable(false);

        usernameColumn.setComparator(Comparator.comparing(user -> user.getUsername().toLowerCase()));
        firstNameColumn.setComparator(Comparator.comparing(user -> user.getFirstName().toLowerCase()));
        lastNameColumn.setComparator(Comparator.comparing(user -> user.getLastName().toLowerCase()));

        HeaderRow filterRow = grid.appendHeaderRow();

        TextField usernameField = new TextField();
        usernameField.addValueChangeListener(event -> {
                userSearchFilter.setUsernameFilterLike(usernameField.getValue());
                cdp.refreshAll();
        });
        usernameField.setValueChangeMode(ValueChangeMode.EAGER);
        filterRow.getCell(usernameColumn).setComponent(usernameField);
        usernameField.setSizeFull();
        usernameField.addThemeVariants(TextFieldVariant.LUMO_SMALL);
        usernameField.setPlaceholder("Фильтр");

        TextField firstNameField = new TextField();
        firstNameField.addValueChangeListener(event -> {
            userSearchFilter.setFirstNameFilterLike(firstNameField.getValue());
            cdp.refreshAll();
        });
        firstNameField.setValueChangeMode(ValueChangeMode.EAGER);
        filterRow.getCell(firstNameColumn).setComponent(firstNameField);
        firstNameField.setSizeFull();
        firstNameField.addThemeVariants(TextFieldVariant.LUMO_SMALL);
        firstNameField.setPlaceholder("Фильтр");

        TextField lastNameField = new TextField();
        lastNameField.addValueChangeListener(event -> {
            userSearchFilter.setLastNameFilterLike(lastNameField.getValue());
            cdp.refreshAll();
        });
        lastNameField.setValueChangeMode(ValueChangeMode.EAGER);
        filterRow.getCell(lastNameColumn).setComponent(lastNameField);
        lastNameField.setSizeFull();
        lastNameField.addThemeVariants(TextFieldVariant.LUMO_SMALL);
        lastNameField.setPlaceholder("Фильтр");


        HorizontalLayout layoutAddBtn = new HorizontalLayout();
        layoutAddBtn.setClassName("layout-buttons");
        layoutAddBtn.setWidthFull();

        Button addBtn = new Button("", VaadinIcon.PLUS.create());
        addBtn.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_SUCCESS, ButtonVariant.LUMO_SMALL);
        addBtn.setClassName("user-add-btn");
        addBtn.setWidthFull();
        addBtn.addClickListener(buttonClickEvent -> {
            userForm.loadForCreate();
            setVisible(false);
            userForm.setVisible(true);
        });
        layoutAddBtn.add(addBtn);

        filterRow.getCell(buttonsColumn).setComponent(layoutAddBtn);

        add(grid);

    }

    public void reload() {
        grid.getDataProvider().refreshAll();
    }

    private Stream<User> fetch(Query<User, UserSearchFilter> query) {
        ApplicationContextProvider appContext = new ApplicationContextProvider();
        UserRepository userRepository = appContext.getApplicationContext().getBean(UserRepository.class);

        Map<String, SortOrder> sortOrderMap = new HashMap<>();
        query.getSortOrders().forEach(querySortOrder -> {
            if (querySortOrder.getDirection().equals(SortDirection.ASCENDING))
               sortOrderMap.put(querySortOrder.getSorted(), SortOrder.ASC);
            else if (querySortOrder.getDirection().equals(SortDirection.DESCENDING))
                sortOrderMap.put(querySortOrder.getSorted(), SortOrder.DESC);
            else
                sortOrderMap.put(querySortOrder.getSorted(), SortOrder.NONE);
        });

        Stream<User> userStream = userRepository.search(query.getOffset(), query.getLimit(), sortOrderMap,
                query.getFilter().orElse(null));
        return userStream;
    }

    private Integer count(Query<User, UserSearchFilter> query) {
        ApplicationContextProvider appContext = new ApplicationContextProvider();
        UserRepository userRepository = appContext.getApplicationContext().getBean(UserRepository.class);
        return userRepository.count(query.getFilter().orElse(null));
    }

    private HorizontalLayout createUserButtons(User user) {

        HorizontalLayout layout = new HorizontalLayout();
        layout.setClassName("layout-buttons");

        Button editBtn = new Button("", VaadinIcon.EDIT.create());
        editBtn.addClickListener(buttonClickEvent -> {
            userForm.loadForUpdate(user.getId());
            userForm.setVisible(true);
            setVisible(false);
        });
        editBtn.setClassName("user-edit-btn");
        editBtn.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_SMALL);
        layout.add(editBtn);

        Button deleteBtn = new Button("", VaadinIcon.TRASH.create());
        deleteBtn.addClickListener(buttonClickEvent ->
                deleteConfirmation.open("Вы действиьельно хотите удалить пользователя " + user.getUsername() + "?",
                user));
        deleteBtn.setClassName("user-delete-btn");
        deleteBtn.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_ERROR, ButtonVariant.LUMO_SMALL);
        layout.add(deleteBtn);

        return layout;
    }

    public void deleteUser(User user) {
        ApplicationContextProvider appContext = new ApplicationContextProvider();
        UserRepository userRepository = appContext.getApplicationContext().getBean(UserRepository.class);

        if (CurrentUser.get().equals(user.getId()))
            Notification.show("Нельзя удалить самого себя!", 2000, Notification.Position.MIDDLE);
        else {
            userRepository.delete(user);
            grid.getDataProvider().refreshAll();
        }
    }

}
