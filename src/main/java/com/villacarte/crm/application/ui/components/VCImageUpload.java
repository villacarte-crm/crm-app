package com.villacarte.crm.application.ui.components;

import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.server.StreamResource;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

public class VCImageUpload extends VerticalLayout {

    private Image image;
    private Upload upload;
    private Button button;
    private ByteArrayOutputStream os;
    private EditBlobListener event;
    private byte[] defaultImage;

    public static VCImageUpload create(int side, String buttonCation, EditBlobListener event, byte[] defaultImage,
                                       HasComponents parent) {
        return new VCImageUpload(side, buttonCation, event, defaultImage, parent);
    }

    private VCImageUpload(int side, String buttonCation, EditBlobListener event, byte[] defaultImage, HasComponents parent) {

        this.defaultImage = defaultImage;

        image = new Image();
        image.setMaxWidth(side + "px");
        image.setMaxHeight(side + "px");
        image.setMinWidth(side + "px");
        image.setMinHeight(side + "px");
        add(image);

        button = new Button(buttonCation);
        button.addThemeVariants(ButtonVariant.LUMO_SMALL);
        button.setMinWidth(side + "px");
        button.setMaxWidth(side + "px");

        upload = new Upload((filename, mimeType) -> {
            os = new ByteArrayOutputStream();
            return os;
        });
        upload.setAcceptedFileTypes("image/jpeg", "image/png", "image/gif");
        upload.setDropAllowed(false);
        upload.addSucceededListener(succeededEvent -> {
            image.setVisible(true);
            try {
                ByteArrayInputStream ios = new ByteArrayInputStream(os.toByteArray());
                BufferedImage inputImage = ImageIO.read(ios);

                int imageSide = Math.min(inputImage.getWidth(), inputImage.getHeight());
                int widthFrom = (inputImage.getWidth() - imageSide) / 2;
                int heightFrom = (inputImage.getHeight() - imageSide) / 2;
                inputImage = inputImage.getSubimage(widthFrom, heightFrom, imageSide, imageSide);
                inputImage = resizeTrick(inputImage, side, side);
                os.reset();
                ImageIO.write(inputImage, "jpg", os);
            } catch (Exception e) {
                e.printStackTrace();
            }

            StreamResource sr = new StreamResource("StreamResource_users", (outputStream, vaadinSession) -> {
                byte [] arr = os.toByteArray();
                event.onEdit(arr);
                outputStream.write(arr);
            });

            image.setSrc(sr);
            button.setEnabled(false);
        });

        upload.setUploadButton(button);
        upload.getElement().addEventListener("upload-abort", domEvent -> {
            StreamResource sr = new StreamResource("StreamResource_users", (outputStream, vaadinSession) -> {
                event.onEdit(defaultImage);
                outputStream.write(defaultImage);
            });

            image.setSrc(sr);
            button.setEnabled(true);
        });

        StreamResource sr = new StreamResource("StreamResource_users", (outputStream, vaadinSession) -> {
            outputStream.write(defaultImage);
        });

        image.setSrc(sr);

        add(upload);

        parent.add(this);
    }

    private static BufferedImage resize(BufferedImage image, int width, int height) {
        int type = image.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : image.getType();
        BufferedImage resizedImage = new BufferedImage(width, height, type);
        Graphics2D g = resizedImage.createGraphics();
        g.setComposite(AlphaComposite.Src);
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.drawImage(image, 0, 0, width, height, null);
        g.dispose();
        return resizedImage;
    }

    private static BufferedImage resizeTrick(BufferedImage image, int width, int height) {
        //image = createCompatibleImage(image);
        image = resize(image, width*2, height*2);
        image = blurImage(image);
        return resize(image, width, height);
    }

    public static BufferedImage blurImage(BufferedImage image) {
        float ninth = 1.0f/9.0f;
        float[] blurKernel = {
                ninth, ninth, ninth,
                ninth, ninth, ninth,
                ninth, ninth, ninth
        };

        Map<RenderingHints.Key, Object> map = new HashMap<>();
        map.put(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        map.put(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);
        map.put(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        RenderingHints hints = new RenderingHints(map);
        BufferedImageOp op = new ConvolveOp(new Kernel(3, 3, blurKernel), ConvolveOp.EDGE_NO_OP, hints);
        return op.filter(image, null);
    }

    private static BufferedImage createCompatibleImage(BufferedImage image) {
        GraphicsConfiguration gc = ((Graphics2D)image.getGraphics()).getDeviceConfiguration();
        int w = image.getWidth();
        int h = image.getHeight();
        BufferedImage result = gc.createCompatibleImage(w, h, Transparency.TRANSLUCENT);
        Graphics2D g2 = result.createGraphics();
        g2.drawRenderedImage(image, null);
        g2.dispose();
        return result;
    }

}
