package com.villacarte.crm.application.ui.users;

import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.upload.Receiver;
import com.vaadin.flow.component.upload.SucceededEvent;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.server.StreamResource;
import com.villacarte.crm.application.ui.components.*;
import com.villacarte.crm.application.ui.components.Dimension;
import com.villacarte.crm.application.ui.util.ApplicationContextProvider;
import com.villacarte.crm.domain.model.User;
import com.villacarte.crm.domain.repository.UserRepository;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class UserForm extends VerticalLayout {

    private final static int BIG_PHOTO_SIZE = 200;

    private UsersGrid usersGrid;
    public void setUsersGrid(UsersGrid usersGrid) { this.usersGrid = usersGrid; }

    private TextField username;
    private TextField firstName;
    private TextField lastName;
    private PasswordField password;
    private PasswordField reenterPassword;
    private Label headerText;

    private Button closeBtn;
    private Button saveBtn;

    public enum Purpose {
        FOR_CREATE,
        FOR_UPDATE
    }

    private Purpose purpose;

    private User user;

    public UserForm() {
        setSizeFull();
    }

    public void initUI(User user) {
        removeAll();

        /*MARKUP*/
        VCHorizontalLayout header = VCHorizontalLayout.createMakup(Dimension.FULL, this);
        VCHorizontalLayout headerLeft = VCHorizontalLayout.createContainer(Dimension.FULL, header);
        VCHorizontalLayout headerRight = VCHorizontalLayout.createContainer(Dimension.FULL, header);
        headerRight.setJustifyContentMode(JustifyContentMode.END);

        VCHorizontalLayout columns = VCHorizontalLayout.createMakup(Dimension.FULL, Dimension.FULL, this);
        VCVerticalLayout column1 = VCVerticalLayout.createContainer(Dimension.MIN_CONTENT, Dimension.FULL, columns);
        VCVerticalLayout column2 = VCVerticalLayout.createContainer(Dimension.FULL, Dimension.FULL, columns);
        VCVerticalLayout column3 = VCVerticalLayout.createContainer(Dimension.FULL, Dimension.FULL, columns);
        VCVerticalLayout column4 = VCVerticalLayout.createContainer(Dimension.FULL, Dimension.FULL, columns);

        /*HEADER*/
        VCLabelHeader.create(purpose == Purpose.FOR_UPDATE ? user.getFirstName() + " " + user.getLastName() :
                "Новый пользователь", VCLabelHeader.Size.XLARGE, headerLeft);

        saveBtn = new Button("Сохранить");
        saveBtn.addClickListener(buttonClickEvent -> save());
        saveBtn.addThemeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_SUCCESS, ButtonVariant.LUMO_PRIMARY);
        headerRight.add(saveBtn);

        closeBtn = new Button(VaadinIcon.CLOSE_CIRCLE_O.create());
        closeBtn.addClickListener(buttonClickEvent -> close());
        closeBtn.addThemeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_ERROR, ButtonVariant.LUMO_PRIMARY);
        headerRight.add(closeBtn);

        /*COLUMN1*/
        VCImageUpload imageUpload = VCImageUpload.create(200, "Загрузить фото", user::setBigPhoto,
                user.getBigPhoto(), column1);

        /*COLUMN2*/
        VCLabelHeader.create("Персональная информация", VCLabelHeader.Size.MEDIUM, column2);
        username = VCTextField.create("Имя пользователя", user::setUsername, column2, user.getUsername())
                .setStyle(VCTextField.Style.EDIT).setPattern_("[A-Za-z0-9]*");
        firstName = VCTextField.create("Имя", user::setFirstName, column2, user.getFirstName())
                .setStyle(VCTextField.Style.EDIT);
        lastName = VCTextField.create("Фамилия", user::setLastName, column2, user.getLastName())
                .setStyle(VCTextField.Style.EDIT);

        /*COLUMN3*/
        password = new PasswordField("Пароль");
        password.setRevealButtonVisible(false);
        column3.add(password);

        reenterPassword = new PasswordField("Повторите пароль");
        reenterPassword.setRevealButtonVisible(false);
        column3.add(reenterPassword);

        password.setVisible(false);
        reenterPassword.setVisible(false);

    }

    public void save() {

        if (username.isEmpty()) {
            username.setInvalid(true);
            return;
        }

        if (purpose == Purpose.FOR_CREATE) {
            if (password.isEmpty() || reenterPassword.isEmpty()) {
                password.setInvalid(password.isEmpty());
                reenterPassword.setInvalid(reenterPassword.isEmpty());
                return;
            }

            if (!password.getValue().equals(reenterPassword.getValue())) {
                password.setInvalid(true);
                reenterPassword.setInvalid(true);
                return;
            }

            user.setPassword(password.getValue());
        }

        ApplicationContextProvider appContext = new ApplicationContextProvider();
        UserRepository userRepository = appContext.getApplicationContext().getBean(UserRepository.class);
        userRepository.save(user);

        usersGrid.reload();
        close();
    }

    public void loadForUpdate(String id) {

        ApplicationContextProvider appContext = new ApplicationContextProvider();
        UserRepository userRepository = appContext.getApplicationContext().getBean(UserRepository.class);
        user = userRepository.get(id, true);
        purpose = Purpose.FOR_UPDATE;
        initUI(user);
    }

    public void loadForCreate() {
        this.user = User.Factory.create();
        purpose = Purpose.FOR_CREATE;
        initUI(user);
    }

    public void close() {
        setVisible(false);
        usersGrid.setVisible(true);
    }

}
