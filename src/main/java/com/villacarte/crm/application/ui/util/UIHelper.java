package com.villacarte.crm.application.ui.util;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyNotifier;
import com.vaadin.flow.component.button.Button;

public class UIHelper {

    public static void addKeyPressToLayout (Component container, Button button) {

        container.getChildren().forEach(component -> {
            if (component instanceof KeyNotifier)
                ((KeyNotifier)component).addKeyPressListener(Key.ENTER, (keyPressEvent) -> button.click());
        });

        if (container instanceof KeyNotifier)
            ((KeyNotifier)container).addKeyPressListener(Key.ENTER, (keyPressEvent) -> button.click());

    }

}
