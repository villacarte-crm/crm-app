package com.villacarte.crm.application.ui.layout;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.villacarte.crm.application.ui.main.MainView;
import com.villacarte.crm.application.ui.users.UserAdministrationView;

import java.util.HashMap;

@HtmlImport("styles/menu.html")
public class Menu extends FlexLayout {

    private HashMap<Class<? extends Component>, RouterButton> routerButtons;

    private RouterButton selectedButton = null;

    public Menu () {
        setClassName("menu-bar");

        routerButtons = new HashMap<>();

        final HorizontalLayout top = new HorizontalLayout();
        top.setDefaultVerticalComponentAlignment(Alignment.CENTER);
        top.setClassName("menu-header");
        Label title = new Label("VillaCarte");
        top.add(title);
        add(top);

        addRouterButton(MainView.class, "Главная", VaadinIcon.ACCORDION_MENU.create());
        addRouterButton(UserAdministrationView.class, "Пользователи", VaadinIcon.USERS.create());

    }

    private void addRouterButton(Class<? extends Component> destinationView, String caption, Icon icon) {
        RouterButton routerButton = new RouterButton(destinationView, caption, icon);
        add (routerButton);
        routerButtons.put(destinationView, routerButton);
    }

    public void setSelectedButton(Class<? extends Component> destinationView) {
        if (selectedButton != null)
            selectedButton.setClassName("menu-button");

        selectedButton = routerButtons.get(destinationView);
        selectedButton.setClassName("menu-button-selected");
    }

}
