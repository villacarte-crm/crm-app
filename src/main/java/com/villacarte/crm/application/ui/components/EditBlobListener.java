package com.villacarte.crm.application.ui.components;

@FunctionalInterface
public interface EditBlobListener {
    void onEdit(byte[] value);
}
