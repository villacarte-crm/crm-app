package com.villacarte.crm.application.ui.main;

import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.PWA;
import com.villacarte.crm.application.ui.layout.MainLayout;

@Route(layout = MainLayout.class)
@PWA(name = "Project Base for Vaadin Flow with Spring", shortName = "Project Base")
public class MainView extends VerticalLayout {

    public MainView() {
        H1 caption = new H1("Добро пожаловать на Главную страницу");
        add(caption);



    }

}
