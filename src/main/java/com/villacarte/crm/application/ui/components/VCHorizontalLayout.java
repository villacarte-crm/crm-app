package com.villacarte.crm.application.ui.components;

import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

public class VCHorizontalLayout extends HorizontalLayout implements ManagedStyles {

    private VCHorizontalLayout(String width, String height, HasComponents parent) {
        super();
        if (width != null)
            setWidth(width);
        if (height != null)
            setHeight(height);

        setMargin(0);

        parent.add(this);
    }

    public static VCHorizontalLayout createMakup(String width, String height, HasComponents parent) {
        VCHorizontalLayout component = new VCHorizontalLayout(width, height, parent);
        component.setPadding(0);
        return component;
    }

    public static VCHorizontalLayout createMakup(String width, HasComponents parent) {
        return createMakup(width, null, parent);
    }

    public static VCHorizontalLayout createMakup(HasComponents parent) {
        return createMakup(null, null, parent);
    }

    public static VCHorizontalLayout createContainer(String width, String height, HasComponents parent) {
        VCHorizontalLayout component = new VCHorizontalLayout(width, height, parent);
        component.setPadding(10);
        return component;
    }

    public static VCHorizontalLayout createContainer(String width, HasComponents parent) {
        return createContainer(width, null, parent);
    }

    public static VCHorizontalLayout createContainer(HasComponents parent) {
        return createContainer(null, null, parent);
    }

}
