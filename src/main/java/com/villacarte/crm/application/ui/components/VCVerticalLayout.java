package com.villacarte.crm.application.ui.components;

import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class VCVerticalLayout extends VerticalLayout implements ManagedStyles {

    private VCVerticalLayout(String width, String height, HasComponents parent) {
        super();
        if (width != null)
            setWidth(width);
        if (height != null)
            setHeight(height);

        setMargin(0);

        parent.add(this);
    }

    public static VCVerticalLayout createMakup(String width, String height, HasComponents parent) {
        VCVerticalLayout component = new VCVerticalLayout(width, height, parent);
        component.setPadding(0);
        return component;
    }

    public static VCVerticalLayout createMakup(String width, HasComponents parent) {
        return createMakup(width, null, parent);
    }

    public static VCVerticalLayout createMakup(HasComponents parent) {
        return createMakup(null, null, parent);
    }

    public static VCVerticalLayout createContainer(String width, String height, HasComponents parent) {
        VCVerticalLayout component = new VCVerticalLayout(width, height, parent);
        component.setPadding(10);
        return component;
    }

    public static VCVerticalLayout createContainer(String width, HasComponents parent) {
        return createContainer(width, null, parent);
    }

    public static VCVerticalLayout createContainer(HasComponents parent) {
        return createContainer(null, null, parent);
    }

}
