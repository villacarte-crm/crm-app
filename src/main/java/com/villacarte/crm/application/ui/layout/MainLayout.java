package com.villacarte.crm.application.ui.layout;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.theme.Theme;

@HtmlImport("styles/styles.html")
public class MainLayout extends FlexLayout implements RouterLayout {

    private Menu menu;

    public MainLayout () {
        setSizeFull();
        setClassName("main-layout");
        menu = new Menu();
        add(menu);
    }

    public void setSelectedButton(Class<? extends Component> viewClass) {
        menu.setSelectedButton(viewClass);
    }


}
