package com.villacarte.crm.application.ui.components;

@FunctionalInterface
public interface SetStyle {
    void setStyle();
}
