package com.villacarte.crm.application.ui.layout;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.Icon;

public class RouterButton extends Button {

    public RouterButton(Class<? extends Component> destinationView, String caption, Icon icon) {
        super(caption, icon);
        setClassName("menu-button");
        addClickListener(buttonClickEvent -> getUI().get().navigate(destinationView));
    }


}
