package com.villacarte.crm.application.ui.init;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.server.ServiceInitEvent;
import com.vaadin.flow.server.VaadinServiceInitListener;
import com.villacarte.crm.application.auth.AccessControl;
import com.villacarte.crm.application.auth.AccessControlFactory;
import com.villacarte.crm.application.ui.layout.MainLayout;
import com.villacarte.crm.application.ui.login.LoginScreen;

public class InitListener implements VaadinServiceInitListener {

    @Override
    public void serviceInit(ServiceInitEvent initEvent) {
        final AccessControl accessControl = AccessControlFactory.getInstance().createAccessControl();

        initEvent.getSource().addUIInitListener((uiInitEvent) -> {


                    uiInitEvent.getUI().addAfterNavigationListener(afterNavigationEvent -> {

                        uiInitEvent.getUI().getChildren().forEach(component -> {

                            if (component instanceof MainLayout) {

                                Class<? extends Component> viewClass =
                                        afterNavigationEvent.getSource().getRegistry().getNavigationTarget(
                                                afterNavigationEvent.getLocation().getPath()
                                        ).orElse(null);

                                //((MainLayout)component).setSelectedTab(viewClass);
                                ((MainLayout)component).setSelectedButton(viewClass);
                            }

                        });




                    });

                    uiInitEvent.getUI().addBeforeEnterListener(beforeEnterEvent -> {

                        if (!accessControl.isUserSignedIn() && !LoginScreen.class
                                .equals(beforeEnterEvent.getNavigationTarget()))
                            beforeEnterEvent.rerouteTo(LoginScreen.class);

                    });


                }
        );
    }

}
