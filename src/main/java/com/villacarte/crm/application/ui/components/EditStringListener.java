package com.villacarte.crm.application.ui.components;

@FunctionalInterface
public interface EditStringListener {
    void onEdit(String param);
}
