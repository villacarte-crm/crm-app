package com.villacarte.crm.application.ui.layout;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.tabs.Tab;

public class RouterTab extends Tab {

    HorizontalLayout routerLayout;

    private Class<? extends Component> viewClass;

    public Class<? extends Component> getRouteView() { return viewClass; }

    public RouterTab(Class<? extends Component> viewClass, String caption, Icon icon) {
        super();

        this.viewClass = viewClass;
        routerLayout = new HorizontalLayout();
        routerLayout.add(icon);
        routerLayout.add(new Span(caption));
        add(routerLayout);

    }
}
