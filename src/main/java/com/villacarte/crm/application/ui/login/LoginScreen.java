package com.villacarte.crm.application.ui.login;

import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.theme.Theme;

@Route("login")
@PageTitle("Login")
@HtmlImport("styles/styles.html")
public class LoginScreen extends VerticalLayout {

    public LoginScreen () {
        setAlignItems(Alignment.CENTER);
        setJustifyContentMode(JustifyContentMode.CENTER);
        setSizeFull();
        add(new LoginPanel());
    }

}
