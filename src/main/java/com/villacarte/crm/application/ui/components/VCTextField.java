package com.villacarte.crm.application.ui.components;

import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.textfield.TextFieldVariant;
import com.vaadin.flow.shared.Registration;

public class VCTextField extends TextField implements ManagedStyles {

    private EditStringListener editStringListener;

    private SetStyle drawStyleEnabled;
    private SetStyle drawStyleDisabled;

    public enum Style {
        EDIT
    }
    private Style style;

    private VCTextField(String name, EditStringListener editStringListener, HasComponents parent) {
        super(name);
        addEditStringListener(editStringListener);
        parent.add(this);
    }

    public static VCTextField create(String name, EditStringListener editStringListener, HasComponents parent,
                                     String initValue) {
        VCTextField component = new VCTextField(name, editStringListener, parent);
        if (initValue != null)
            component.setValue(initValue);
        return component;
    }

    public void addEditStringListener(EditStringListener editStringListener) {
        super.addValueChangeListener(event -> {
            if (editStringListener != null)
                editStringListener.onEdit(event.getValue());
        });
        this.editStringListener = editStringListener;
    }

    @Override
    public Registration addValueChangeListener(ValueChangeListener<? super ComponentValueChangeEvent<TextField, String>> listener) {

        if (this.editStringListener != null)
            return super.addValueChangeListener(event -> {
                listener.valueChanged(event);
                editStringListener.onEdit(event.getValue());
            });
        else
            return super.addValueChangeListener(listener);

    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (enabled)
            drawStyleEnabled.setStyle();
        else
            drawStyleDisabled.setStyle();
    }

    public VCTextField setStyle(Style style) {

        switch (style) {
            case EDIT: {
                drawStyleEnabled = this::drawStyleEditEnabled;
                drawStyleDisabled = this::drawStyleEditDisabled;
            }
        }

        setEnabled(isEnabled());

        return this;
    }

    private void drawStyleEdit() {
        addThemeVariants(TextFieldVariant.LUMO_SMALL);
        setPadding(0);
        setMargin(5, 0, 0, 0);
        setWidthFull();
    }

    private void drawStyleEditEnabled() {
        drawStyleEdit();
    }

    private void drawStyleEditDisabled() {
        drawStyleEdit();
    }

    public VCTextField setPattern_(String pattern) {
        setPattern(pattern);
        return this;
    }

}
