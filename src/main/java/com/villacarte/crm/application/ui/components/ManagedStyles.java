package com.villacarte.crm.application.ui.components;

import com.vaadin.flow.component.HasStyle;

public interface ManagedStyles extends HasStyle {

    default void setBackground(String color) {
        getStyle().set("bacground", color);
    }

    default void setColor(String color) {
        getStyle().set("color", color);
    }

    default void setMargin(int top, int right, int bottom, int left) {
        getStyle().set("margin", top + "px " + right + "px " + bottom + "px " + left + "px");
    }

    default void setMargin(int margin) {
        setMargin(margin, margin, margin, margin);
    }

    default void setPadding(int top, int right, int bottom, int left) {
        getStyle().set("padding", top + "px " + right + "px " + bottom + "px " + left + "px");
    }

    default void setPadding(int padding) {
        setPadding(padding, padding, padding, padding);
    }

}
