package com.villacarte.crm.application.ui.login;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.Autocomplete;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.villacarte.crm.application.ui.util.ApplicationContextProvider;
import com.villacarte.crm.application.auth.AccessControl;
import com.villacarte.crm.application.ui.util.UIHelper;

public class LoginPanel extends VerticalLayout {

    private TextField username;
    private PasswordField password;
    private Button loginBtn;

    public LoginPanel () {

        setAlignItems(Alignment.CENTER);
        setMaxWidth("300px");

        username = new TextField("Имя пользователя");
        username.setWidthFull();
        username.setClassName("text-field");
        username.setAutocomplete(Autocomplete.USERNAME);
        username.setRequired(true);
        add(username);

        password = new PasswordField("Пароль");
        password.setWidthFull();
        password.setClassName("text-field");
        password.setRequired(true);
        add(password);

        loginBtn = new Button("Войти");
        loginBtn.setWidthFull();
        loginBtn.setClassName("button");
        loginBtn.addClickListener(this::loginClick);
        add(loginBtn);

        UIHelper.addKeyPressToLayout(this, loginBtn);

    }

    private void loginClick(ClickEvent<Button> event) {

        if (username.isEmpty() || password.isEmpty()) {
            username.setInvalid(username.isEmpty());
            password.setInvalid(password.isEmpty());
            return;
        }

        ApplicationContextProvider appContext = new ApplicationContextProvider();
        AccessControl accessControl = appContext.getApplicationContext().getBean(AccessControl.class);

        if (accessControl.signIn(username.getValue(), password.getValue())) {
            getUI().get().navigate("");
        } else {

            Label notificationLabel = new Label("Неверный логин или пароль");
            notificationLabel.setClassName("notification_alert");

            Notification notification = new Notification();
            notification.setDuration(3000);
            notification.add(notificationLabel);
            notification.setPosition(Notification.Position.BOTTOM_CENTER);
            notification.open();
        }

    }

}
