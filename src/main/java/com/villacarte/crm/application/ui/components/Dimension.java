package com.villacarte.crm.application.ui.components;

public class Dimension {

    public final static String FULL = "100%";
    public final static String MIN_CONTENT = "min-content";

}
