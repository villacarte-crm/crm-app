package com.villacarte.crm.application.auth;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.server.VaadinSession;
import com.villacarte.crm.domain.model.User;
import com.villacarte.crm.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

/**
 * Default mock implementation of {@link AccessControl}. This implementation
 * accepts any string as a password, and considers the user "admin" as the only
 * administrator.
 */
@Service
@Scope("singleton")
public class BasicAccessControl implements AccessControl {

    @Autowired
    UserRepository userRepository;

    @Override
    public boolean signIn(String username, String password) {
        if (username == null || username.isEmpty())
            return false;

        User user = userRepository.findByUsername(username);

        if (user == null)
            return false;

        if (!user.checkPassword(password))
            return false;
        else {
            CurrentUser.set(user.getId());
            return true;
        }
    }

    @Override
    public boolean isUserSignedIn() {
        return !CurrentUser.get().isEmpty();
    }

    @Override
    public boolean isUserInRole(String role) {
        if ("admin".equals(role)) {
            // Only the "admin" user is in the "admin" role
            return getPrincipalName().equals("admin");
        }

        // All users are in all non-admin roles
        return true;
    }

    @Override
    public String getPrincipalName() {
        return CurrentUser.get();
    }

    @Override
    public void signOut() {
        VaadinSession.getCurrent().getSession().invalidate();
        UI.getCurrent().getPage().reload();
    }
}
