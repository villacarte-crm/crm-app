package com.villacarte.crm.application.init;

import com.villacarte.crm.domain.service.InitChecks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

@Controller
public class InitProcedures {

    @Autowired
    InitChecks initChecks;

    public void init() {

        initChecks.checkUsers();

    }

}
