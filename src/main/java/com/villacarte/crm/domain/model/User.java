package com.villacarte.crm.domain.model;

import org.springframework.util.DigestUtils;

import java.util.Date;
import java.util.UUID;

public class User {

    /*ID*/
    private String id;
    public final String getId() { return id; }

    /*CREATED*/
    protected Date created;
    public final Date getCreated() { return created; }

    /*USERNAME*/
    private String username;
    public String getUsername() { return username; }
    public void setUsername(String username) { this.username = username; }

    /*PASSWORD*/
    private String passwordHash;
    private String hashPassword(String password) { return DigestUtils.md5DigestAsHex(password.getBytes()); }
    public String getPasswordHash() { return passwordHash; }
    public void setPasswordHash(String passwordHash) { this.passwordHash = passwordHash; }
    public boolean checkPassword (String password) {
        if (passwordHash == null)
            return false;
        else
            return passwordHash.equals(hashPassword(password));
    }
    public void setPassword (String password) {
        if (password != null)
            passwordHash = hashPassword(password);
        else
            throw new IllegalArgumentException("Password cannot bu null");
    }

    /*FIRST NAME*/
    private String firstName;
    public String getFirstName() { return firstName; }
    public void setFirstName(String firstName) { this.firstName = firstName; }

    /*LAST NAME*/
    private String lastName;
    public String getLastName() { return lastName; }
    public void setLastName(String lastName) { this.lastName = lastName; }

    /*BIG PHOTO*/
    private byte[] bigPhoto;
    public byte[] getBigPhoto() { return bigPhoto; }
    public void setBigPhoto(byte[] bigPhoto) { this.bigPhoto = bigPhoto; }

    /*SMALL PHOTO*/
    private byte[] smallPhoto;
    public byte[] getSmallPhoto() { return smallPhoto; }
    public void setSmallPhoto(byte[] smallPhoto) { this.smallPhoto = smallPhoto; }

    /*EMAIL*/
    private String email;
    public String getEmail() { return email; }
    public void setEmail(String email) { this.email = email; }

    /*MOBILE PHONE*/
    private String mobilePhone;
    public String getMobilePhone() { return mobilePhone; }
    public void setMobilePhone(String mobilePhone) { this.mobilePhone = mobilePhone; }

    /*VOIP NUMBER*/
    private String voipNumber;
    public String getVoipNumber() { return voipNumber; }
    public void setVoipNumber(String voipNumber) { this.voipNumber = voipNumber; }

    /*OCCUPATION*/
    private String Occupation;
    public String getOccupation() { return Occupation; }
    public void setOccupation(String occupation) { Occupation = occupation; }

    /*CONSTRUCTOR*/
    protected User () {}

    /*FACTORY*/
    public static class Factory {

        public static User create () {
            User user = new User();
            user.id = UUID.randomUUID().toString();
            user.created = new Date();
            return user;
        }

        public static User create (String username) {
            User user = create();
            user.username = username;
            return user;
        }

        public static User restore (String id, String username, Date created) {
            User user = new User();
            user.username = username;
            user.created = created;
            user.id = id;
            return user;
        }

    }

}
