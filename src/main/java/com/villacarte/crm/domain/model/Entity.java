package com.villacarte.crm.domain.model;

import java.util.Date;

public abstract class Entity {

    protected Date created;
    public final Date created() { return created; }

    protected User createdBy;
    public final User createdBy() { return createdBy; }

    protected String id;
    public final String getId() { return id; }

    @Override
    public final boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        return id.equals(((Entity)obj).id);
    }

    @Override
    public final int hashCode() {
        return id.hashCode();
    }

}
