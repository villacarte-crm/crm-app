package com.villacarte.crm.domain.repository.common;

public class IdSearchParam extends SearchParam {

    private String searchId;
    public String getSearchId() { return searchId; }
    public void setSearchId(String searchId) { this.searchId = searchId; }

    private IdSearchParam() {}
    private IdSearchParam(String param, String searchId) {
        this.param = param;
        this.searchId = searchId;
    }

    public static IdSearchParam eq(String param, String searchId) {
        return new IdSearchParam(param, searchId);
    }

}
