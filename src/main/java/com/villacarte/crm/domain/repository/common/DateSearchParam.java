package com.villacarte.crm.domain.repository.common;

import java.util.Date;

public class DateSearchParam extends SearchParam {

    public enum SearchType {
        EXACT,
        AFTER,
        BEFORE,
        PERIOD
    }

    private SearchType searchType;
    public SearchType getSearchType() { return searchType; }
    public void setSearchType(SearchType searchType) { this.searchType = searchType; }

    private Date singleDate;
    public Date getSingleDate() { return singleDate; }
    public void setSingleDate(Date singleDate) { this.singleDate = singleDate; }

    private Date afterDate;
    public Date getAfterDate() { return afterDate; }
    public void setAfterDate(Date afterDate) { this.afterDate = afterDate; }

    private Date beforeDate;
    public Date getBeforeDate() { return beforeDate; }
    public void setBeforeDate(Date beforeDate) { this.beforeDate = beforeDate; }

    private DateSearchParam() {}
    private DateSearchParam(String param, SearchType searchType, Date singleDate, Date afterDate, Date beforeDate) {
        this.param = param;
        this.searchType = searchType;
        this.singleDate = singleDate;
        this.afterDate = afterDate;
        this.beforeDate = beforeDate;
    }

    public static DateSearchParam eq(String param, Date date) {
        return new DateSearchParam(param, SearchType.EXACT, date, null, null);
    }

    public static DateSearchParam after(String param, Date date) {
        return new DateSearchParam(param, SearchType.AFTER, null, date, null);
    }

    public static DateSearchParam before(String param, Date date) {
        return new DateSearchParam(param, SearchType.BEFORE, null, null, date);
    }

    public static DateSearchParam period(String param, Date dateAfter, Date dateBefore) {
        return new DateSearchParam(param, SearchType.PERIOD, null, dateAfter, dateBefore);
    }
}
