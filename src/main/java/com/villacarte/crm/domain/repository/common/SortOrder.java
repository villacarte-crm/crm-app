package com.villacarte.crm.domain.repository.common;

public enum SortOrder {
    NONE,
    ASC,
    DESC
}
