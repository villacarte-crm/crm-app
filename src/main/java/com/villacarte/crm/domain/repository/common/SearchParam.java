package com.villacarte.crm.domain.repository.common;

public abstract class SearchParam {

    protected String param;
    public String getParam() { return param; }
    public void setParam(String param) { this.param = param; }

}
