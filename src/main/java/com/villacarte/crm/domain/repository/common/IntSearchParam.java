package com.villacarte.crm.domain.repository.common;

public class IntSearchParam extends SearchParam {

    public enum SearchType {
        EQUALS,
        MORE,
        LESS
    }

    private SearchType searchType;
    public SearchType getSearchType() { return searchType; }
    public void setSearchType(SearchType searchType) { this.searchType = searchType; }

    private int searchValue;
    public int getSearchValue() { return searchValue; }
    public void setSearchValue(int searchValue) { this.searchValue = searchValue; }

    private IntSearchParam() {}
    private IntSearchParam(String param, SearchType searchType, int searchValue) {
        this.searchType = searchType;
        this.searchValue = searchValue;
    }

    public static IntSearchParam eq(String param, int searchValue) {
        return new IntSearchParam(param, SearchType.EQUALS, searchValue);
    }

    public static IntSearchParam more(String param, int searchValue) {
        return new IntSearchParam(param, SearchType.MORE, searchValue);
    }

    public static IntSearchParam less(String param, int searchValue) {
        return new IntSearchParam(param, SearchType.LESS, searchValue);
    }
}
