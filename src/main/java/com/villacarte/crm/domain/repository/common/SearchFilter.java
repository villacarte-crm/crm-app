package com.villacarte.crm.domain.repository.common;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class SearchFilter {

    private Map<String, SearchParam> searchParams;
    public Map<String, SearchParam> getSearchParams() { return searchParams; }

    protected SearchFilter() {
        searchParams = new HashMap<>();
    }

    protected void setIdFilter(@NotNull @NotEmpty String param, String value) {
        if (value == null || value.isEmpty())
            searchParams.remove(param);
        else
            searchParams.put(param, IdSearchParam.eq(param, value));
    }

    protected void setStringFilterEquals(@NotNull @NotEmpty String param, String value) {
        if (value == null || value.isEmpty())
            searchParams.remove(param);
        else
            searchParams.put(param, StringSearchParam.eq(param, value));
    }

    protected void setStringFilterLike(@NotNull @NotEmpty String param, String value) {
        if (value == null || value.isEmpty())
            searchParams.remove(param);
        else
            searchParams.put(param, StringSearchParam.like(param, value));
    }

    protected void setDateFilterEquals(@NotNull @NotEmpty String param, Date value) {
        if (value == null)
            searchParams.remove(param);
        else
            searchParams.put(param, DateSearchParam.eq(param, value));
    }

    protected void setDateFilterGreater(@NotNull @NotEmpty String param, Date value) {
        if (value == null)
            searchParams.remove(param);
        else
            searchParams.put(param, DateSearchParam.after(param, value));
    }

    protected void setDateFilterLess(@NotNull @NotEmpty String param, Date value) {
        if (value == null)
            searchParams.remove(param);
        else
            searchParams.put(param, DateSearchParam.before(param, value));
    }

    protected void setDateFilterBetween(@NotNull @NotEmpty String param, Date valueFrom, Date valueTo) {
        if (valueFrom == null &&  valueTo != null)
            setDateFilterLess(param, valueTo);
        else if (valueFrom != null &&  valueTo == null)
            setDateFilterGreater(param, valueFrom);
        else if (valueFrom == null &&  valueTo == null)
            searchParams.remove(param);
        else
            searchParams.put(param, DateSearchParam.period(param, valueFrom, valueTo));
    }

    protected void setIntFilterEquals(@NotNull @NotEmpty String param, Integer value) {
        if (value == null)
            searchParams.remove(param);
        else
            searchParams.put(param, IntSearchParam.eq(param, value));
    }

    protected void setIntFilterGreater(@NotNull @NotEmpty String param, Integer value) {
        if (value == null)
            searchParams.remove(param);
        else
            searchParams.put(param, IntSearchParam.more(param, value));
    }

    protected void setIntFilterLess(@NotNull @NotEmpty String param, Integer value) {
        if (value == null)
            searchParams.remove(param);
        else
            searchParams.put(param, IntSearchParam.less(param, value));
    }

    protected void remove(SearchParam param) {
        searchParams.remove(param.getParam());
    }

    protected void remove(String paramName) {
        searchParams.remove(paramName);
    }

    protected void clear() {
        searchParams.clear();
    }

}
