package com.villacarte.crm.domain.repository;

import com.villacarte.crm.domain.model.User;
import com.villacarte.crm.domain.repository.common.SortOrder;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public interface UserRepository {

    User get(String id, boolean withPhoto);

    User findByUsername(String username);

    List<User> findAll();

    List<User> search(String searchString);

    void save(User user);

    void delete(User user);

    Stream<User> search(int offset, int limit, Map<String, SortOrder> sortOrders);

    Stream<User> search(int offset, int limit, Map<String, SortOrder> sortOrders, UserSearchFilter filter);

    Integer count(UserSearchFilter filter);

}
