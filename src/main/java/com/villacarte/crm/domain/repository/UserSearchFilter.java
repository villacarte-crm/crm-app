package com.villacarte.crm.domain.repository;

import com.villacarte.crm.domain.repository.common.SearchFilter;

public class UserSearchFilter extends SearchFilter {

    private UserSearchFilter() {
        super();
    }

    public static UserSearchFilter create() {
        return new UserSearchFilter();
    }

    public UserSearchFilter setIdFilter(String value) {
        setIdFilter("id", value);
        return this;
    }

    public UserSearchFilter setUsernameFilterEqual(String username) {
        setStringFilterEquals("username", username);
        return this;
    }

    public UserSearchFilter setUsernameFilterLike(String username) {
        setStringFilterLike("username", username);
        return this;
    }

    public UserSearchFilter setFirstNameFilterEquals(String value) {
        setStringFilterEquals("firstName", value);
        return this;
    }

    public UserSearchFilter setFirstNameFilterLike(String value) {
        setStringFilterLike("firstName", value);
        return this;
    }

    public UserSearchFilter setLastNameFilterEquals(String value) {
        setStringFilterEquals("lastName", value);
        return this;
    }

    public UserSearchFilter setLastNameFilterLike(String value) {
        setStringFilterLike("lastName", value);
        return this;
    }

}
