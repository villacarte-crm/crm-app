package com.villacarte.crm.domain.repository.common;

public class StringSearchParam extends SearchParam {

    public enum SearchType {
        EQUALS,
        LIKE
    }

    private SearchType searchType;
    public SearchType getSearchType() { return searchType; }
    public void setSearchType(SearchType searchType) { this.searchType = searchType; }

    private String searchString;
    public String getSearchString() { return searchString; }
    public void setSearchString(String searchString) { this.searchString = searchString; }

    private StringSearchParam() {}
    private StringSearchParam(String param, SearchType searchType, String searchString) {
        this.param = param;
        this.searchType = searchType;
        this.searchString = searchString;
    }

    public static StringSearchParam eq(String param, String searchString) {
        return new StringSearchParam(param, SearchType.EQUALS, searchString);
    }

    public static StringSearchParam like(String param, String searchString) {
        return new StringSearchParam(param, SearchType.LIKE, searchString);
    }

}
